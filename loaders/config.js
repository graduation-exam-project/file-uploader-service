const config = {
  func: () => {},
  static: {
    name: "file-uploader-service",
    port: "8001",
    method: "POST"
  }
};

module.exports = config;
