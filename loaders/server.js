const App = require("./app");
const config = require("./config");
const http = require("http");

class Server {
  constructor() {
    this.app = new App(config).express;
    this.server = http.createServer(this.app);
    this.port = config.static.port;
    this._init();
    this._listener();
  }

  _init() {
    this.app.set("port", this.port);

    this.server.listen(this.port);
  }

  _listener() {
    this.server.on("listening", () => {
      let addr = this.server.address();
      let bind =
        typeof addr === "string" ? `pipe ${addr}` : `port ${addr.port}`;
      console.log(`Listening on ${bind}`);
    });
  }
}

module.exports = new Server();
