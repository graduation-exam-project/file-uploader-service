const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const multer = require("multer");
const path = require("path");

const filetypes = /jpeg|jpg|png|gif|txt|pdf|docx/;

const storage = multer.diskStorage({
  destination: "../assets",
  filename: function(req, file, cb) {
    cb(
      null,
      file.fieldname + "-" + Date.now() + path.extname(file.originalname)
    );
  }
});

const upload = multer({
  storage: storage,
  limits: { fileSize: 1000000 },
  fileFilter: function(req, file, cb) {
    checkFileType(file, cb);
  }
}).single("file");

const checkFileType = (file, cb) => {
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  const mimetype = filetypes.test(file.mimetype);

  if (mimetype && extname) {
    return cb(null, true);
  } else {
    cb("No supported file type");
  }
};

class App {
  constructor(config) {
    this.config = config.static;
    this.func = config.func;
    this.express = express();
    this._middleware();
    this._router();
  }

  _middleware() {
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
    this.express.use(
      cors({
        origin: ["*"],
        methods: ["GET", this.config.method.toLowerCase()]
      })
    );
  }

  _router() {
    this.express.get("/", (req, res, next) => {
      res.send(`Running ${this.config.name} on port: ${this.config.port}.`);
    });

    this.express[this.config.method.toLowerCase()]("/api", (req, res, next) => {
      upload(req, res, err => {
        if (err) {
          res.send(JSON.stringify({ status: false, code: err }));
        } else {
          if (req.file == undefined) {
            res.send(JSON.stringify({ status: false, code: "No such a file" }));
          } else {
            console.log(req.file);
            res.send(
              JSON.stringify({
                status: true,
                code: "File uploaded",
                data: { fileName: req.file.filename }
              })
            );
          }
        }
      });
    });
  }
}

module.exports = App;
